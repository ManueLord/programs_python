save = {}
res = ""
finish = []
equation = []
operation = False

def result(problem, count, csignal):
    global operation, res
    if operation == True:
        operation = False
    else:
        res += problem[count]
    count += 1
    if ord(problem[count]) >= 97 and ord(problem[count]) <= 123:
        result(problem, count, csignal)
    elif problem[count] == '+' or problem[count] == '*' or problem[count] == '-' or problem[count] == '/' or problem[count] == '^':
        operators(problem, count, csignal)
    elif problem[count] == '(' or problem[count] == ')':
        parentheses(problem, count, csignal)

def operators(problem, count, csignal):
    global operation
    x = problem[count]
    save[csignal] += x[::-1]
    operation = True
    result(problem, count, csignal)

def parentheses(problem, count, csignal) :
    global operation, res
    if problem[count] == '(':
        csignal += 1
        save[csignal] = ""
        count += 1
        if ord(problem[count]) >= 97 and ord(problem[count]) <= 123:
            result(problem, count, csignal) 
        elif problem[count] == '(':
            parentheses(problem, count, csignal)
    elif problem[count] == ')':
        operation = True
        res += save[csignal]
        save.pop(csignal)
        csignal -= 1
        if count == len(problem)-1:
            finish.append(res)
            res = ""
        else:
            result(problem, count, csignal)
    else:
        print("Bad")

how = int(input("How many operations: "))

for i in range(how):
    equation.append(input())

for i in equation:
    parentheses(i, 0, 0)
    

for i in finish:
    print(i)

